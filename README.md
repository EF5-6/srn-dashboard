# A web dashboard to visualize sequential road networks

You can [try this dashboard online](https://wsgi.math.tu-berlin.de/roadnets/). It is also embedded [here](https://www3.math.tu-berlin.de/disco/research/projects/evolution-models/).

## Dependencies

The dashboard requires **Python 3** and the following packages:

- `dash`
- `more_itertools`
- `networkx`
- `numpy`
- `plotly`
- `scipy`

To host this application in a production environment, a [WSGI](https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface)-capable webserver is required.

## Usage

The dashboard can be tested locally by executing `app.py` and accessing the URL printed in a web browser (typically http://localhost:8050).

For a production environment, you might need to create a configuration file `app.wsgi` with the following content (adapt to your needs):

```python
#!/usr/bin/env python3

import os
import sys

os.environ["DASH_REQUESTS_PATHNAME_PREFIX"] = "/application_name/"
sys.path.insert(0, "/path/to/repository")

import app

application = app.app.server
```

## License

The source code is licensed under the [GNU GPL v3](LICENSE.txt).

The data in the [assets/locations](assets/locations) subfolder is made available under the [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license.

## Citing

The following [article](https://doi.org/10.1093/pnasnexus/pgac313) introduces sequential road networks and mentions this dashboard.

```bibtex
@article{SSDK23,
  author  = {Maximilian J. Stahlberg and Guillaume Sagnol and Benjamin Ducke and Max Klimm},
  title   = {Spatiotemporal reconstruction of ancient road networks through sequential cost--benefit analysis},
  journal = {PNAS Nexus},
  volume  = {2},
  number  = {2},
  pages   = {1--10},
  year    = {2023},
  doi     = {10.1093/pnasnexus/pgac313}
}
```
