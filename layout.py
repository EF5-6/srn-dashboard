#!/usr/bin/env python3
# pylama:ignore=D103

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This file is part of SRN Dashboard.
#
# SRN Dashboard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SRN Dashboard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Layout for a web dashboard for exploring sequential road networks."""

import random

from dash import dcc, html

import locations
from config import (
    HIDDEN,
    MAX_SEED,
    MIN_SEED,
    SHOWN,
    STAT_OPTIONS,
    SORTED_ORDER_TYPES,
)


# =============================================================================
# Component types
# =============================================================================


def ControlBox(**kwargs):
    return html.Div(className="app-control-box", **kwargs)


def FlexControl(**kwargs):
    return html.P(className="app-control-flex", **kwargs)


def FlexControlHeader(**kwargs):
    return html.H3(className="app-control-flex", **kwargs)


# =============================================================================
# Components
# =============================================================================


# Selection
# -----------------------------------------------------------------------------

selection_close_button = html.Button(
    "Close",
    id="selection-close-button",
    n_clicks=0,
)

selection = ControlBox(
    id="selection",
    children=[
        FlexControlHeader(
            children=[
                html.Label(id="selection-title"),
                selection_close_button,
            ],
        ),
        html.Div(id="selection-info"),
    ],
    style=HIDDEN,
)

# Terrain
# -----------------------------------------------------------------------------

terrain_type = dcc.RadioItems(
    id="terrain-type",
    className="app-control-radiochecklist",
    options=[
        {"label": "Artificial", "value": "artificial"},
        {"label": "Real World", "value": "real"},
    ],
    value="artificial",
)

terrain_class = dcc.Dropdown(
    id="terrain-class",
    options=[
        {"label": "Delaunay Triangulation", "value": "delaunay"},
        {"label": "2D Grid", "value": "grid"},
        {"label": "Triangular Tiling", "value": "triangular"},
        {"label": "Hexagonal Tiling", "value": "hexagonal"},
        {"label": "Complete Graph", "value": "complete"},
    ],
    clearable=False,
    value="delaunay",
)

terrain_resolution = dcc.Slider(
    id="terrain-resolution",
    className="app-control-slider",
    min=2,
    max=50,
    step=1,
    value=30,
    marks=None,
    tooltip={"placement": "right", "always_visible": True},
)

terrain_sites = dcc.Slider(
    id="terrain-sites",
    className="app-control-slider",
    min=2,
    max=50,
    step=1,
    value=20,
    marks=None,
    tooltip={"placement": "right", "always_visible": True},
)

terrain_seed = dcc.Input(
    id="terrain-seed",
    type="number",
    min=MIN_SEED,
    max=MAX_SEED,
    value=random.randint(MIN_SEED, MAX_SEED),
    size="9",
)

terrain_seed_button = html.Button(
    "Generate",
    id="terrain-seed-button",
    n_clicks=0,
)

terrain_location = dcc.Dropdown(
    id="terrain-location",
    options=[
        {"label": location, "value": location}
        for location in locations.get_locations()
    ],
    clearable=False,
)

terrain = ControlBox(
    children=[
        html.H3("Terrain"),
        FlexControl(
            children=[html.Label("Type:"), terrain_type],
        ),
        html.Div(
            id="terrain-type--artificial",
            children=[
                FlexControl(
                    children=[html.Label("Class:"), terrain_class],
                ),
                html.P([html.Label("Resolution:"), terrain_resolution]),
                html.P([html.Label("Sites:"), terrain_sites]),
                FlexControl(
                    children=[
                        html.Label("Random seed:"),
                        html.Span([terrain_seed, terrain_seed_button]),
                    ],
                ),
            ],
            style=SHOWN,
        ),
        html.Div(
            id="terrain-type--real",
            children=[
                FlexControl(
                    children=[
                        html.Label("Location:"),
                        terrain_location,
                    ],
                )
            ],
            style=HIDDEN,
        ),
    ],
)

# Roads
# -----------------------------------------------------------------------------

road_mode = dcc.RadioItems(
    id="road-mode",
    className="app-control-radiochecklist",
    options=[
        {"label": "All Pairs", "value": "all_pairs"},
        {"label": "Single Source", "value": "single"},
    ],
    value="all_pairs",
)

road_alpha = dcc.Slider(
    id="road-alpha",
    className="app-control-slider",
    updatemode="drag",
    min=0,
    max=1,
    step=0.05,
    value=0.5,
    marks=None,
    tooltip={"placement": "right", "always_visible": True},
)

road_order = dcc.Dropdown(
    id="road-order",
    options=[
        dict(
            value="precomputed",
            label="Precomputed",
            title="Choose a connection order stored on the server.",
        ),
        dict(
            value="random",
            label="Random",
            title="Generate a connection order at random.",
        ),
    ]
    + [dict(value=k, **v) for k, v in SORTED_ORDER_TYPES.items()],
    clearable=False,
    value="random",
)

road_seed = dcc.Input(
    id="road-seed",
    type="number",
    min=MIN_SEED,
    max=MAX_SEED,
    value=random.randint(MIN_SEED, MAX_SEED),
    size="9",
)

road_seed_button = html.Button(
    "Generate",
    id="road-seed-button",
    n_clicks=0,
)

road_plan = dcc.Dropdown(
    id="road-plan",
    options=[],
    clearable=False,
)

roads = ControlBox(
    children=[
        html.H3("Roads"),
        html.P([html.Label("Alpha:"), road_alpha]),
        FlexControl(
            children=[html.Label("Order:"), road_order],
        ),
        html.Div(
            id="road-order--mode",
            children=[
                FlexControl(
                    children=[html.Label("Connect:"), road_mode],
                ),
            ],
            style=SHOWN,
        ),
        html.Div(
            id="road-order--random",
            children=[
                FlexControl(
                    children=[
                        html.Label("Random seed:"),
                        html.Span([road_seed, road_seed_button]),
                    ],
                ),
            ],
            style=SHOWN,
        ),
        html.Div(
            id="road-order--precomputed",
            children=[
                FlexControl(
                    children=[html.Label("Plan:"), road_plan],
                ),
            ],
            style=HIDDEN,
        ),
    ],
)

# Display
# -----------------------------------------------------------------------------

display_site_stat = dcc.Dropdown(
    id="display-site-stat",
    options=[
        dict(label=v["label"], title=v["title"], value=k)
        for k, v in STAT_OPTIONS.items()
    ],
    clearable=True,
    value="first_pos",
)

display_options = dcc.Checklist(
    id="display-options",
    className="app-control-radiochecklist",
    options=[
        {"label": "Terrain", "value": "show_terrain"},
        {"label": "Sites", "value": "show_sites"},
        {"label": "Reference", "value": "show_reference", "disabled": True},
        {"label": "Detail", "value": "show_detail", "disabled": True},
    ],
    value=["show_reference", "show_sites", "show_detail"],
)

display = ControlBox(
    children=[
        html.H3("Display"),
        FlexControl(
            children=[
                html.Label("Site info:"),
                display_site_stat,
            ],
        ),
        FlexControl(
            children=[html.Label("Show:"), display_options],
        ),
    ],
)

# Graph
# -----------------------------------------------------------------------------

graph = dcc.Graph(
    id="graph",
    className="figure",
    responsive=True,
    config=dict(displaylogo=False),
)

# Intermediates
# -----------------------------------------------------------------------------

sites_trace_num = dcc.Store(id="sites-trace-num", data=None)
selected_site = dcc.Store(id="selected-site", data=None)
site_info = dcc.Store(id="site-info", data=None)

intermediates = [sites_trace_num, selected_site, site_info]


# =============================================================================
# Layout
# =============================================================================


LAYOUT = html.Div(
    className="app-body",
    children=[
        html.Div(
            className="app-controls",
            children=[selection, terrain, roads, display],
        ),
        html.Div(className="app-output", children=[graph]),
        *intermediates,
    ],
)
