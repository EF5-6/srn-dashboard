#!/usr/bin/env python3
# pylama:ignore=D103

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This file is part of SRN Dashboard.
#
# SRN Dashboard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SRN Dashboard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Application logic for a dashboard for exploring sequential road networks."""

from functools import lru_cache
from itertools import combinations

import networkx as nx
import numpy as np
import scipy.spatial
from more_itertools import pairwise

from config import (
    BASE_COST_KEY,
    COORDS_KEY,
    IS_SITE_KEY,
    RDCD_COST_KEY,
    ROAD_TIER_KEY,
    ROAD_TIME_KEY,
    SORTED_ORDER_TYPES,
)


def _cost_function(base, tier, alpha):
    """Compute travel cost based on base cost and road tier."""
    if tier > 0:
        return alpha * base
    else:
        return base


@lru_cache(1)
def make_network(terrain_class, resolution, seed):
    np.random.seed(seed)

    n = resolution

    if terrain_class == "grid":
        network = nx.grid_2d_graph(range(n), range(n))

        for v in network.nodes:
            network.nodes[v][COORDS_KEY] = np.array(v)
    elif terrain_class == "triangular":
        network = nx.triangular_lattice_graph(n, int(n * 3**0.5))

        for _, d in network.nodes(data=True):
            d[COORDS_KEY] = np.array(d["pos"])
    elif terrain_class == "hexagonal":
        network = nx.hexagonal_lattice_graph(int(n / 2 * 3**0.5), n)

        for _, d in network.nodes(data=True):
            d[COORDS_KEY] = np.array(d["pos"])
    elif terrain_class == "delaunay":
        points = np.random.random((n**2, 2))
        triangulation = scipy.spatial.Delaunay(points)

        network = nx.Graph()
        for path in triangulation.simplices:
            nx.add_path(network, path)

        for v in network.nodes:
            network.nodes[v][COORDS_KEY] = triangulation.points[v, :]
    elif terrain_class == "complete":
        points = np.random.random((2 * n, 2))

        network = nx.complete_graph(2 * n)

        for i, v in enumerate(network.nodes):
            network.nodes[v][COORDS_KEY] = points[i, :]
    else:
        raise NotImplementedError("Unknown terrain type.")

    nx.set_edge_attributes(
        network,
        {
            (u, v): np.linalg.norm(
                network.nodes[u][COORDS_KEY] - network.nodes[v][COORDS_KEY]
            )
            for u, v in network.edges
        },
        BASE_COST_KEY,
    )

    return network


@lru_cache(1)
def place_sites(network, count, seed):
    network = network.copy()

    np.random.seed(seed)

    count = min(count, len(network))
    nums = np.random.choice(len(network), count, False)

    sites = []
    for i, v in enumerate(network.nodes):
        if i in nums:
            sites.append(v)
            network.nodes[v][IS_SITE_KEY] = True
        else:
            network.nodes[v][IS_SITE_KEY] = False

    return network


@lru_cache(1)
def _sites_and_connections(network, mode, selected_site):
    # Get a list of sites.
    sites = [v for v, d in network.nodes(data=True) if d[IS_SITE_KEY]]

    # Get a list of connections in base order.
    if mode is None or mode == "all_pairs":
        connections = list(combinations(sites, 2))
    elif mode == "single":
        assert selected_site is not None

        rome = sites[selected_site]
        connections = [
            (rome, v) for i, v in enumerate(sites) if i != selected_site
        ]
    else:
        raise ValueError("Unknown road mode.")

    return sites, connections


@lru_cache(len(SORTED_ORDER_TYPES))
def make_sorted_order(network, mode, selected_site, sorting):
    # Get sites and connections in base order.
    sites, connections = _sites_and_connections(network, mode, selected_site)

    if sorting in ("increasing_distance", "decreasing_distance"):
        decreasing = sorting == "decreasing_distance"

        distances = dict()
        for s in sites:
            for t, d in nx.single_source_dijkstra_path_length(
                network, s, weight=BASE_COST_KEY
            ).items():
                distances[s, t] = d

        order = tuple(
            sorted(
                range(len(connections)),
                key=lambda i: distances[connections[i]],
                reverse=decreasing,
            )
        )
    else:
        raise ValueError("Unknown road order.")

    return order


@lru_cache(1)
def build_roads(network, mode, order, alpha, seed, selected_site):
    network = network.copy()

    if seed is not None:
        np.random.seed(seed)

    # Prepare edge attributes.
    nx.set_edge_attributes(network, 0, ROAD_TIER_KEY)
    nx.set_edge_attributes(
        network,
        {
            (u, v): _cost_function(network[u][v][BASE_COST_KEY], 0, alpha)
            for u, v in network.edges()
        },
        RDCD_COST_KEY,
    )
    nx.set_edge_attributes(network, float("inf"), ROAD_TIME_KEY)

    # Prepare node attributes.
    nx.set_node_attributes(network, float("inf"), "first_pos")
    nx.set_node_attributes(network, 0, "cost_contributed")
    nx.set_node_attributes(
        network, {v: set() for v in network}, "segments_used"
    )
    nx.set_node_attributes(
        network, {v: set() for v in network}, "segments_built"
    )

    # Get sites and connections in base order.
    sites, connections = _sites_and_connections(network, mode, selected_site)

    # Decide on a connection order.
    if order is None:
        order = connections.copy()
        np.random.shuffle(order)
    else:
        # Convert order from indices to connections.
        order = [connections[i] for i in order]

    # Establish the connections.
    for i, (u, v) in enumerate(order, 1):
        # Find shortest u-v-path.
        path_nodes = nx.shortest_path(network, u, v, RDCD_COST_KEY)

        # Update the network.
        for e in pairwise(path_nodes):
            a, b = e
            d = network[a][b]
            e = tuple(sorted(e))

            if not d[ROAD_TIER_KEY]:
                for w in (u, v):
                    dw = network.nodes[w]
                    dw["cost_contributed"] += d[BASE_COST_KEY] / 2.0
                    dw["segments_built"].add(e)

            network.nodes[u]["segments_used"].add(e)
            network.nodes[v]["segments_used"].add(e)

            d[ROAD_TIER_KEY] += 1
            d[RDCD_COST_KEY] = _cost_function(
                d[BASE_COST_KEY], d[ROAD_TIER_KEY], alpha
            )
            d[ROAD_TIME_KEY] = min(d[ROAD_TIME_KEY], i)

        # Save node statistics.
        for a in (u, v):
            d = network.nodes[a]
            d["first_pos"] = min(d["first_pos"], i)

    # Make statistics serializable.
    for v in sites:
        for key in ("segments_built", "segments_used"):
            network.nodes[v][key] = tuple(sorted(network.nodes[v][key]))

    return network
