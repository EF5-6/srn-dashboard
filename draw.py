#!/usr/bin/env python3
# pylama:ignore=D103

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This file is part of SRN Dashboard.
#
# SRN Dashboard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SRN Dashboard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Drawing functions for a dashboard for exploring sequential road networks."""

from functools import lru_cache

import plotly.graph_objects as go

from config import (
    COORDS_KEY,
    IS_SITE_KEY,
    ROAD_TIER_KEY,
    ROAD_TIME_KEY,
    SITE_NAME_KEY,
    STAT_OPTIONS,
    VERTICES_KEY,
)


@lru_cache()
def _get_edge_coords(
    network,
    *,
    detail=False,
    selector=None,
    reverse=False,
    edge_filter=None,
    by_time=False,
):
    if by_time:
        history = {}
    else:
        x_seq, y_seq = [], []

    for u, v, d in network.edges(data=True):
        if selector and bool(d[selector]) is bool(reverse):
            continue

        if (
            edge_filter is not None
            and (u, v) not in edge_filter
            and (v, u) not in edge_filter
        ):
            continue

        if by_time:
            t = d[ROAD_TIME_KEY]
            history.setdefault(t, ([], []))
            x_seq = history[t][0]
            y_seq = history[t][1]

        if detail and VERTICES_KEY in d:
            vertices = d[VERTICES_KEY]
            x, y = zip(*vertices)

            x_seq.extend(x)
            y_seq.extend(y)

            x_seq.append(None)
            y_seq.append(None)
        else:
            ux, uy = network.nodes[u][COORDS_KEY]
            vx, vy = network.nodes[v][COORDS_KEY]

            x_seq.extend((ux, vx, None))
            y_seq.extend((uy, vy, None))

    if by_time:
        return history
    else:
        return x_seq, y_seq


def _make_annotation_text(t=float("inf")):
    if t == float("inf"):
        return ""

    return f"{t if t else 'No'} connection{'' if t == 1 else 's'} established"


def make_figure(
    network,
    site_stat,
    options,
    selected_site,
    single_source_mode,
    reference_network,
):
    traces = []
    trace_time = []
    sites_trace_num = None

    # Get a list of sites.
    site_data = [d for v, d in network.nodes(data=True) if d[IS_SITE_KEY]]

    detail = "show_detail" in options

    # Draw reference network, if any.
    if reference_network and "show_reference" in options:
        reference_coords = _get_edge_coords(reference_network, detail=detail)

        traces.append(
            go.Scatter(
                mode="lines",
                x=reference_coords[0],
                y=reference_coords[1],
                line=dict(width=4, color="#ffc285"),
                hoverinfo="skip",
            )
        )
        trace_time.append(0)

    # Draw terrain.
    if "show_terrain" in options:
        unused_coords = _get_edge_coords(network, detail=detail)

        traces.append(
            go.Scatter(
                mode="lines",
                x=unused_coords[0],
                y=unused_coords[1],
                line=dict(width=0.5, color="#888"),
                hoverinfo="skip",
            )
        )
        trace_time.append(0)

    # Draw roads.
    if selected_site is None or single_source_mode:
        history = _get_edge_coords(
            network, detail=detail, selector=ROAD_TIER_KEY, by_time=True
        )

        for t, (x, y) in history.items():
            traces.append(
                go.Scatter(
                    mode="lines",
                    x=x,
                    y=y,
                    line_color="#444",
                    line_width=2,
                    hoverinfo="skip",
                )
            )
            trace_time.append(t)
    else:
        d = site_data[selected_site]

        built = frozenset(d["segments_built"])
        used = frozenset(d["segments_used"])

        rest = set()
        for u, v, d in network.edges(data=True):
            if not d[ROAD_TIER_KEY]:
                continue

            e = tuple(sorted((u, v)))

            if e not in used:
                rest.add(e)
        rest = frozenset(rest)

        colors = ("#444", "#444", "#666")
        widths = (4, 2, 2)
        dashes = ("solid", "solid", "dot")

        for edges, color, width, dash in zip(
            (built, used - built, rest), colors, widths, dashes
        ):
            history = _get_edge_coords(
                network, detail=detail, edge_filter=edges, by_time=True
            )

            for t, (x, y) in history.items():
                traces.append(
                    go.Scatter(
                        mode="lines",
                        x=x,
                        y=y,
                        line=dict(color=color, width=width, dash=dash),
                        hoverinfo="skip",
                    )
                )
                trace_time.append(t)

    # Draw sites.
    if "show_sites" in options:
        site_coords = ([], [])
        values, tooltips = [], []

        if site_stat:
            stat_label = STAT_OPTIONS[site_stat]["label"]
            stat_format = STAT_OPTIONS[site_stat]["format"]

            colorbar = dict(
                x=1.0,
                thickness=16,
                title=dict(text=stat_label, side="right"),
                tickangle=-90,
                ticklabelposition="inside",
                outlinewidth=0,
                ypad=0,
            )
        else:
            colorbar = None

        for _, d in network.nodes(data=True):
            if not d[IS_SITE_KEY]:
                continue

            x, y = d[COORDS_KEY]
            site_coords[0].append(x)
            site_coords[1].append(y)

            name = (
                d[SITE_NAME_KEY]
                if SITE_NAME_KEY in d
                else f"Site #{len(values) + 1}"
            )

            if site_stat:
                value = d[site_stat]
                values.append(value)
                tooltips.append(
                    f"<b>{name}</b><br />{stat_label}: {value:{stat_format}}"
                )
            else:
                tooltips.append(name)

        sites_trace_num = len(traces)

        traces.append(
            go.Scatter(
                mode="markers",
                x=site_coords[0],
                y=site_coords[1],
                marker=dict(
                    symbol=[
                        200 if i == selected_site else 0
                        for i in range(len(network.nodes))
                    ],
                    size=[
                        12 if i == selected_site else 10
                        for i in range(len(network.nodes))
                    ],
                    line_width=2,
                    line_color="#444",
                    opacity=1,
                    showscale=bool(site_stat),
                    colorscale="PinkYl",
                    color=values if site_stat else "white",
                    colorbar=colorbar,
                ),
                hoverinfo="text",
                text=tooltips,
            )
        )
        trace_time.append(0)

    # Add a slider.
    time_steps = sorted(set(trace_time)) + [float("inf")]
    road_traces = [i for i, t in enumerate(trace_time) if t]
    sliders = [
        dict(
            active=len(time_steps) - 1,
            currentvalue=dict(visible=False),
            pad=dict(t=3),
            transition=dict(duration=0),
            steps=[
                dict(
                    label=t if t < float("inf") else "All",
                    method="update",
                    args=[
                        {
                            "visible": [
                                trace_time[i] <= t for i in road_traces
                            ],
                            "line.color": [
                                "red" if t and trace_time[i] == t else "#444"
                                for i in road_traces
                            ],
                        },
                        {"annotations[0].text": _make_annotation_text(t)},
                        road_traces,
                    ],
                )
                for t in time_steps
            ],
        )
    ]

    # Show the slider's current value as an in-figure annotation.
    annotations = [
        dict(
            text=_make_annotation_text(),
            font=dict(size=14),
            showarrow=False,
            xref="paper",
            yref="paper",
            x=1 / 200,
            y=0,
        )
    ]

    graph = go.Figure(
        data=traces,
        layout=go.Layout(
            showlegend=False,
            hovermode="closest",
            margin=dict(b=5, l=5, r=5, t=5),
            xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
            yaxis=dict(
                showgrid=False,
                zeroline=False,
                showticklabels=False,
                scaleanchor="x",
            ),
            sliders=sliders,
            annotations=annotations,
        ),
    )

    return graph, sites_trace_num
