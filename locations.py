#!/usr/bin/env python3
# pylama:ignore=D103

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This file is part of SRN Dashboard.
#
# SRN Dashboard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SRN Dashboard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""A manager for pickled networks created from GIS data."""

import json
import pickle
from functools import lru_cache
from pathlib import Path

ROOT = Path("assets/locations")

TERRAIN_FILE = "terrain.pickle"
REFERENCE_FILE = "reference.pickle"
PLAN_DIR = "plans"
PLAN_EXT = ".plan.json"


if not ROOT.is_dir():
    raise RuntimeError(f"Directory {ROOT} not found.")


def get_locations():
    return tuple(
        segment.name for segment in ROOT.iterdir() if segment.is_dir()
    )


@lru_cache()
def load_terrain(location):
    if location not in get_locations():
        raise ValueError("Bad location request.")

    terrain_file = ROOT / location / TERRAIN_FILE

    if not terrain_file.is_file():
        raise LookupError(f"No terrain file found for location {location}.")

    with open(terrain_file, "rb") as fp:
        return pickle.load(fp)


@lru_cache()
def load_reference(location):
    if location not in get_locations():
        raise ValueError("Bad location request.")

    reference_file = ROOT / location / REFERENCE_FILE

    if not reference_file.is_file():
        raise LookupError(f"No reference file found for location {location}.")

    with open(reference_file, "rb") as fp:
        return pickle.load(fp)


def get_plans(location):
    if location not in get_locations():
        raise ValueError("Bad location request.")

    plan_dir = ROOT / location / PLAN_DIR

    if not plan_dir.is_dir():
        raise LookupError(f"No plan folder found for location {location}.")

    return tuple(
        sorted(
            segment.name
            for segment in plan_dir.iterdir()
            if segment.is_file() and segment.name.endswith(PLAN_EXT)
        )
    )


@lru_cache()
def load_plan(location, plan):
    if location not in get_locations():
        raise ValueError("Bad location request.")

    if plan not in get_plans(location):
        raise ValueError("Bad plan request.")

    plan_file = ROOT / location / PLAN_DIR / plan

    assert plan_file.is_file()

    with open(plan_file, "r") as fp:
        plan = json.load(fp)

    alpha = plan["alpha"]
    order = tuple(plan["order"])

    return order, alpha
