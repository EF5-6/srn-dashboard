#!/usr/bin/env python3

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This file is part of SRN Dashboard.
#
# SRN Dashboard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SRN Dashboard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Configuration for a web dashboard for exploring sequential road networks."""

MIN_SEED = 0
MAX_SEED = int(1e8) - 1

HIDDEN = {"display": "none"}
SHOWN = {"display": "block"}

COORDS_KEY = "_coords"
VERTICES_KEY = "_vertices"
IS_SITE_KEY = "is_site"
SITE_NAME_KEY = "site_name"
BASE_COST_KEY = "cost"
RDCD_COST_KEY = "reduced_cost"
ROAD_TIER_KEY = "road_tier"
ROAD_TIME_KEY = "road_time"

STAT_OPTIONS = {
    "first_pos": dict(
        label="First connection time",
        title="First occurrence of the site in the connection order.",
        format="d",
    ),
    "cost_contributed": dict(
        label="Road cost contributed",
        title=(
            "Half of the base costs of road segments created for connections "
            "involving the site."
        ),
        format=".2e",
    ),
}

SORTED_ORDER_TYPES = {
    "increasing_distance": dict(
        label="Increasing Distance",
        title="Connect close sites first, remote sites last.",
    ),
    "decreasing_distance": dict(
        label="Decreasing Distance",
        title="Connect remote sites first, close sites last.",
    ),
}
