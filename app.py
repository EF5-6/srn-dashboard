#!/usr/bin/env python3
# pylama:ignore=D103

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This file is part of SRN Dashboard.
#
# SRN Dashboard is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SRN Dashboard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""A web dashboard for exploring sequential road networks."""

import random

import dash
from dash import Input, Output, State, html, no_update
from dash.exceptions import PreventUpdate

import locations
from config import (
    HIDDEN,
    IS_SITE_KEY,
    MAX_SEED,
    MIN_SEED,
    SHOWN,
    SITE_NAME_KEY,
    SORTED_ORDER_TYPES,
    STAT_OPTIONS,
)
from draw import make_figure
from layout import LAYOUT, FlexControl
from logic import build_roads, make_network, make_sorted_order, place_sites

app = dash.Dash("roadnets", title="Sequential Road Network Simulator")
app.layout = LAYOUT


# =============================================================================
# Selection callbacks
# =============================================================================


@app.callback(
    Output("selection", "style"),
    Output("selection-title", "children"),
    Output("selection-info", "children"),
    Input("site-info", "data"),
    Input("selected-site", "data"),
)
def update_selection(site_info, selected_site):
    if selected_site is None:
        return HIDDEN, "", []

    info = site_info[selected_site]
    name = (
        info[SITE_NAME_KEY]
        if SITE_NAME_KEY in info
        else f"Site #{selected_site + 1}"
    )

    children = [
        FlexControl(
            children=[
                html.Label(f"{stat['label']}:"),
                f"{site_info[selected_site][key]:{stat['format']}}",
            ]
        )
        for key, stat in STAT_OPTIONS.items()
    ]

    return SHOWN, name, children


@app.callback(
    Output("selection-close-button", "disabled"),
    Input("road-order", "value"),
    Input("road-mode", "value"),
)
def update_selection_close_button(road_order, road_mode):
    return road_order != "precomputed" and road_mode == "single"


# =============================================================================
# Terrain callbacks
# =============================================================================


@app.callback(
    Output("terrain-type--artificial", "style"),
    Output("terrain-type--real", "style"),
    Input("terrain-type", "value"),
)
def unroll_terrain_type(terrain_type):
    return (
        SHOWN if terrain_type == "artificial" else HIDDEN,
        SHOWN if terrain_type == "real" else HIDDEN,
    )


@app.callback(
    Output("terrain-seed", "value"), Input("terrain-seed-button", "n_clicks")
)
def update_terrain_seed(n_clicks):
    return random.randint(MIN_SEED, MAX_SEED)


# =============================================================================
# Road callbacks
# =============================================================================


@app.callback(
    Output("road-order", "value"),
    Output("road-order", "options"),
    Input("terrain-type", "value"),
    State("road-order", "value"),
    State("road-order", "options"),
)
def update_road_order(terrain_type, old_value, old_options):
    is_real = terrain_type == "real"

    if not is_real and old_value == "precomputed":
        value = "random"
    else:
        value = old_value

    options = old_options.copy()

    for num, option in enumerate(old_options):
        if option["value"] == "precomputed":
            options[num] = option.copy()
            options[num]["disabled"] = not is_real

    return value, options


@app.callback(
    Output("road-order--mode", "style"),
    Output("road-order--random", "style"),
    Output("road-order--precomputed", "style"),
    Input("road-order", "value"),
)
def unroll_road_order(order):
    return (
        SHOWN if order != "precomputed" else HIDDEN,
        SHOWN if order == "random" else HIDDEN,
        SHOWN if order == "precomputed" else HIDDEN,
    )


@app.callback(
    Output("road-plan", "options"),
    Input("terrain-location", "value"),
)
def update_road_plans(location):
    if location is None:
        raise PreventUpdate()

    return [
        {"label": plan, "value": plan}
        for plan in locations.get_plans(location)
    ]


@app.callback(
    Output("road-alpha", "value"),
    Input("road-plan", "value"),
    State("terrain-location", "value"),
)
def update_road_alpha(plan, location):
    if location is None or plan is None:
        raise PreventUpdate()

    the_plan = locations.load_plan(location, plan)

    _, alpha = the_plan

    return alpha


@app.callback(
    Output("road-seed", "value"), Input("road-seed-button", "n_clicks")
)
def update_road_seed(n_clicks):
    return random.randint(MIN_SEED, MAX_SEED)


# =============================================================================
# Display callbacks
# =============================================================================


@app.callback(
    Output("display-options", "options"),
    Input("terrain-type", "value"),
    State("display-options", "options"),
)
def update_display_options(terrain_type, old_options):
    options = old_options.copy()

    for num, option in enumerate(old_options):
        if option["value"] in ("show_reference", "show_detail"):
            options[num] = option.copy()
            options[num]["disabled"] = terrain_type != "real"

    return options


# =============================================================================
# Graph callbacks
# =============================================================================


@app.callback(
    Output("graph", "figure"),
    Output("site-info", "data"),
    Output("sites-trace-num", "data"),
    Input("terrain-type", "value"),
    Input("terrain-class", "value"),
    Input("terrain-location", "value"),
    Input("terrain-resolution", "value"),
    Input("terrain-sites", "value"),
    Input("terrain-seed", "value"),
    Input("road-mode", "value"),
    Input("road-alpha", "value"),
    Input("road-order", "value"),
    Input("road-plan", "value"),
    Input("road-seed", "value"),
    Input("display-site-stat", "value"),
    Input("display-options", "value"),
    Input("selected-site", "data"),
)
def update_graph(
    terrain_type,
    terrain_class,
    terrain_location,
    terrain_resolution,
    terrain_sites,
    terrain_seed,
    road_mode,
    road_alpha,
    road_order,
    road_plan,
    road_seed,
    display_site_stat,
    display_options,
    selected_site,
):
    if terrain_type == "artificial":
        network = make_network(terrain_class, terrain_resolution, terrain_seed)
        network = place_sites(network, terrain_sites, terrain_seed)

        reference_network = None
    elif terrain_type == "real":
        if terrain_location is None:
            raise PreventUpdate()

        network = locations.load_terrain(terrain_location)

        try:
            reference_network = locations.load_reference(terrain_location)
        except LookupError:
            reference_network = None
    else:
        raise ValueError(f"Unknown terrain type {terrain_type}.")

    if road_order == "precomputed":
        if terrain_type != "real":
            raise PreventUpdate()

        if road_plan is None:
            raise PreventUpdate()

        order, _ = locations.load_plan(terrain_location, road_plan)

        network = build_roads(
            network=network,
            mode=None,
            order=order,
            alpha=road_alpha,
            seed=None,
            selected_site=None,
        )
    else:
        if road_order == "random":
            order = None
        elif road_order in SORTED_ORDER_TYPES:
            order = make_sorted_order(
                network, road_mode, selected_site, road_order
            )
        else:
            raise ValueError(f"Unknown road order {road_order}.")

        network = build_roads(
            network=network,
            mode=road_mode,
            order=order,
            alpha=road_alpha,
            seed=road_seed,
            selected_site=selected_site,
        )

    figure, sites_trace_num = make_figure(
        network=network,
        site_stat=display_site_stat,
        options=display_options,
        selected_site=selected_site,
        single_source_mode=(
            road_order != "precomputed" and road_mode == "single"
        ),
        reference_network=reference_network,
    )

    site_info = []
    for v, d in network.nodes(data=True):
        if d[IS_SITE_KEY]:
            site_info.append(network.nodes[v])

    return figure, site_info, sites_trace_num


# =============================================================================
# Intermediate callbacks
# =============================================================================


@app.callback(
    Output("selected-site", "data"),
    Input("selection-close-button", "n_clicks"),
    Input("graph", "clickData"),
    Input("terrain-sites", "value"),
    Input("road-order", "value"),
    Input("road-mode", "value"),
    State("sites-trace-num", "data"),
    State("selected-site", "data"),
)
def update_selected_site(
    close_button,
    click_data,
    terrain_sites,
    road_order,
    road_mode,
    sites_trace_num,
    old_selection,
):
    # Enforce selection in single-source mode.
    needed = road_order != "precomputed" and road_mode == "single"

    ctx = dash.callback_context

    if not ctx.triggered:
        return 0 if needed else None

    trigger = ctx.triggered[0]["prop_id"]

    if trigger == "selection-close-button.n_clicks":
        return no_update if needed else None
    elif trigger == "graph.clickData":
        point = click_data["points"][0]

        if point["curveNumber"] != sites_trace_num:
            return no_update

        selected = point["pointNumber"]

        if selected == old_selection:
            return no_update if needed else None
        else:
            return selected
    elif trigger == "terrain-sites.value":
        if old_selection is not None and old_selection >= terrain_sites:
            return terrain_sites - 1
        else:
            return no_update
    elif trigger in ("road-order.value", "road-mode.value"):
        return 0 if (needed and old_selection is None) else no_update
    else:
        assert False, "Unexpected trigger."


if __name__ == "__main__":
    app.run_server(debug=True)
